require('dotenv').config();

const TelegramApi = require('node-telegram-bot-api');
const token = process.env.TOKEN;

const {gameOptions, againOptions} = require('./options');

bot = new TelegramApi(token, {polling: true});

const chats = {};

const startGame = async (chatId) => {
    await bot.sendMessage(chatId, `Бот загадает цифру от 0 до 9, попробуйте отгадать ее!`);
    const randomNumber = Math.floor(Math.random() * 10);
    console.log(randomNumber);
    chats[chatId] = randomNumber;
    await bot.sendMessage(chatId, `Отгадывай!`, gameOptions);
}

const start = () => {
    bot.setMyCommands([
        {command: '/start', description: 'Начальное приветствие'},
        {command: '/info', description: 'Информация о вас'},
        {command: '/game', description: 'Сыграть в игру'},
    ])
    
    bot.on('message', async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;
        if (text === '/start') {
            await bot.sendSticker(chatId, 'https://en.wikifur.com/w/images/6/6d/Cheetah_hi_sticker.webp');
            return bot.sendMessage(chatId, `Добро пожаловать в телеграм бота, ${msg.chat.first_name}`);
        }
        if (text === '/info') {
            return bot.sendMessage(chatId, `Мы знаем вас как ${msg.chat.first_name}`);
        }
        if (text === '/game') {
            startGame(chatId);
        }
        return bot.sendMessage(chatId, `Я вас не понимаю`);
    })

    bot.on('callback_query', async msg => {
        const data = msg.data;
        const chatId = msg.message.chat.id;
        if (data == '/again') {
            return startGame(chatId);
        }
        if (data == chats[chatId]) {
            return bot.sendMessage(chatId, `Ура! Вы отгадали цифру ${data}`, againOptions);
        }
        else {
            return bot.sendMessage(chatId, 'Увы, не верно, попробуйте еще раз!', againOptions);
        }
    })
}

start()